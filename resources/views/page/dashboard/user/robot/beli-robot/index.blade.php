@extends('layouts.dash')
@section('script-top')
    <title>List Robot</title>


    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('script-bottom')
    <!-- DataTables -->
    <script src="{{ asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}">
    </script>
    <!-- page script -->
    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Beli Robot
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <?php
            $background_colors = ['aqua', 'green', 'red', 'yellow', 'white', 'warning'];

            $rand_background = $background_colors[array_rand($background_colors)];
            ?>
            @foreach ($data as $item)
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-{{ $rand_background }}">
                        <div class="inner">
                            <h3>{{ $item->name }}</h3>

                            <p>${{ $item->price }}</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-outlet"></i>
                        </div>
                        <a href="{{ route('dashboard.customer.beli-robot.show', $item->id) }}"
                            class="small-box-footer">Lihat Detail<i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endforeach
            <!-- ./col -->
        </div>

        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
