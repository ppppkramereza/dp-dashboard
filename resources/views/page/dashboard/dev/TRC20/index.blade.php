@extends('layouts.dash')
<title>Dashboard | TRC</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            TRC
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">TRC</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">TRC20</h3>

            </div>
            <!-- /.box-header -->
            <div class="">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>Details</th>

                        </tr>
                    </thead>
        <tbody>
            @foreach ($data as $i => $value)


            <tr>
                <td>{{$value->transaction_id}}</td>
                <td><a style="" class="btn btn-info" data-toggle="modal" data-target="#modal-detail{{$value->transaction_id}}"><i class="fa fa-pencil"></i>

            </tr>
            @endforeach
        </tbody>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- DataTables -->
    @foreach ($data as $i => $value)
    <div class="modal fade" id="modal-detail{{$value->transaction_id}}" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Details</h4>
          </div>
          <div class="modal-body">

            <div class="box-body">
                <div class="form-group">
                    <label>Transaction ID</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->transaction_id}}" placeholder="Enter ..." disabled>
                  </div>

                <div class="form-group">
                    <label>Block Timestamp</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->block_timestamp}} {{\Carbon\Carbon::now()->timestamp }}" placeholder="Enter ..." disabled>
                  </div>
                <div class="form-group">
                    <label>From</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->from}}" placeholder="Enter ..." disabled>
                  </div>
                <div class="form-group">
                    <label>To</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->to}}" placeholder="Enter ..." disabled>
                  </div>
                <div class="form-group">
                    <label>Type</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->type}}" placeholder="Enter ..." disabled>
                  </div>
                <div class="form-group">
                    <label>Value</label>
                    <input style="max-width: 100%" type="text" class="form-control" value="{{$value->value}}" placeholder="Enter ..." disabled>
                  </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="button" class="btn btn-default"  data-dismiss="modal">Done</button>
            </div>
            <!-- /.box-footer -->
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    @endforeach
@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <!-- CK Editor -->
    <script src="{{asset('assets/AdminLTE/bower_components/ckeditor/ckeditor.js')}}"></script>

   <script>
       $(document).ready(function() {
    var table = $('#example').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
   </script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true,
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })

      $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
    //bootstrap WYSIHTML5 - text editor
  })
    </script>
@endsection
