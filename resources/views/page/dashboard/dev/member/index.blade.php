@extends('layouts.dash')
<title>Dashboard | Manajemen User</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Manajemen User
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Manajemen User</a></li>
        <li class="active">Member</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasilupdate')){ @endphp
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      @php if(Session::get('gagal')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Email atau No HP sudah pernah terdaftar
      </div> @php } @endphp
      @php if(Session::get('gagalrefferal')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Kode Refferal tidak ditemukan
      </div> @php } @endphp
      @php if(Session::get('berhasil')){ @endphp
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Member</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
                <a data-toggle="modal" data-target="#modal-tambahmodal" style="margin-left: 13px"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Modal User</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Telp</th>
                  <th>Level</th>
                  <th>Status</th>
                  <th>Modal User (USDT)</th>
                  <th>Saldo Wallet Biasa(USDT)</th>
                  <th>Tgl Daftar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->email}}</td>
                  <td>{{$dt->country_code}}{{$dt->phone}}</td>
                  <td>{{$dt->leveluser->name}}</td>
                  <td>{{$dt->statususer->name}}</td>
                  <td>{{$dt->modal_user}}</td>
                  <td>{{$dt->wallet->balance}}</td>
                  <td>{{$dt->created_at}}</td>
                  <td>
                  <a class="btn btn-warning" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil" ></i></a>

                  <a class="btn btn-warning" data-toggle="modal" data-target="#modal-upline{{$dt->id}}"><i class="fa fa-level-up"></i> </a>

                  <a class="btn btn-warning" data-toggle="modal" data-target="#modal-password{{$dt->id}}"><i class="fa fa-key"></i> </a>
                      <a href="/dashboard/dev/member/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                      </a>

                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!--
      Modal -->
      @foreach ($datas as $dt)
      <div class="modal fade" id="modal-edit{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Profil Member</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.member.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                    <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="profil" required="" >
                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Michael Sumadi" required="" value="{{$dt->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="myemail@email.com" required="" value="{{$dt->email}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="country_code" class="col-sm-2 control-label">Phone</label>
                  <div class="col-sm-4">
                    @php
                      $country = DB::select("select * from phone_countries");
                    @endphp
                    <select class="form-control select2" name="country_code" id="country_code" style="width: 100%;">
                      <!-- <option selected="selected">Alabama</option> -->
                      @foreach ($country as $dts)
                        @if ($dts->iso_code == $dt->country_code)
                          <option value="{{$dts->iso_code}}" selected="">{{$dts->iso_code}}</option>
                        @else if ($dts ->iso_code != $dt->country_code)
                          <option value="{{$dts->iso_code}}">{{$dts->iso_code}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <input type="number" class="form-control" id="phone" placeholder="9877788" name="phone" required="" value="{{$dt->phone}}" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>

      <div class="modal fade" id="modal-password{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Password Member {{$dt->name}}</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.member.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password Baru</label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                    <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="password" required="" >
                    <input type="text" class="form-control" id="password" name="password" placeholder="MyPassword" required="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>



      <div class="modal fade" id="modal-upline{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Upline Member {{$dt->name}}</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.member.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="upline" class="col-sm-2 control-label">Pilih Upline</label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                    <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="upline" required="" >
                    @php
                      $user = DB::select("select * from users");
                      $upline = DB::select("select * from uplines where user_id='$dt->id'");
                      $id_upline = "";
                        foreach($upline as $up){
                            $id_upline = $up->upline_id;
                        }
                    @endphp
                    <select class="form-control select2" name="upline" id="upline" style="width: 100%;">
                      @foreach ($user as $dtaa)
                        @if ($id_upline == $dtaa->id)
                          <option value="{{$dtaa->id}}" selected="">{{$dtaa->name}}</option>
                        @else if ($id_upline != $dtaa->id)
                          <option value="{{$dtaa->id}}" >{{$dtaa->name}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      @endforeach

       <div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Member</h4>
            </div>
            <div class="modal-body">

             <form class="form-horizontal" action="{{ route('dashboard.dev.member.store') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Michael Sumadi" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="myemail@email.com" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="refferal" class="col-sm-2 control-label">Refferal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="refferal" placeholder="Your Ref" name="refferal" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-10 control-label">Phone</label>
                  <div class="col-sm-4">
                    @php
                    $data = DB::select("select * from phone_countries");
                    @endphp
                    <select class="form-control select2" name="country_code" id="country_code">
                      @foreach ($data as $dt)
                      <option value="{{$dt->iso_code}}">{{$dt->iso_code}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <input type="number" class="form-control" id="phone" placeholder="876666" name="phone" required="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>

      <div class="modal fade" id="modal-tambahmodal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Modal User</h4>
            </div>
            <div class="modal-body">

             <form class="form-horizontal" action="{{ route('dashboard.dev.topupmt5.tambahmodal') }}" method="post">
            @csrf
              <div class="box-body">

                <div class="form-group">
                  <label for="id" class="col-sm-2 control-label">Nama Member</label>
                  <div class="col-sm-10" style="margin-top: 12px">
                    @php
                    $data = DB::select("select * from users");
                    @endphp
                    <select class="form-control select2" name="id" id="id" style="width: 100%;">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">USD</a></li>
                      <li><a href="#tab_2" data-toggle="tab">IDR</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <div class="form-group">
                            <label for="modal" class="col-sm-2 control-label">Modal User (USD)</label>
                            <div class="col-sm-10">
                              <input type="number" step="0.01" class="form-control" id="modal" name="modal" placeholder="100" style="margin-top: 18px">
                            </div>
                        </div>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <div class="form-group">
                            <label for="modalrupiah" class="col-sm-2 control-label">Modal User (IDR)</label>
                            <div class="col-sm-10">
                              <input type="number" class="form-control" id="modalrupiah" name="modalrupiah" placeholder="100" style="margin-top: 18px">
                            </div>
                        </div>
                      </div>

                    </div>
                    <!-- /.tab-content -->
                  </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>

    <script>
        $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        })
    </script>
    <script src="{{asset('assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
@endsection
