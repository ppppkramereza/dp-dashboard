@extends('layouts.dash')
<title>Dashboard | Manajemen User</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Bypass Robot
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Bypass Robot</li>
      </ol>

    <section class="content">
      @php if(Session::get('berhasilupdate')){ @endphp
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      @php if(Session::get('gagal')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Email sudah pernah terdaftar
      </div> @php } @endphp
      @php if(Session::get('berhasil')){ @endphp
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp

<div class="container-fluid">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Bypass</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->

                <form action="{{ route('api.belirobotbypass') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="id">Nama</label>

                              <select class="form-control select2" name="user_id" id="id" style="width: 100%;">
                                @foreach ($users as $dt)
                                <option value="{{$dt->id}}">{{$dt->name}} - {{$dt->email}}</option>
                                @endforeach
                              </select>

                          </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->

        </div>

    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->

    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
    <script>
        $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        })
    </script>
    <script src="{{asset('assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

@endsection
