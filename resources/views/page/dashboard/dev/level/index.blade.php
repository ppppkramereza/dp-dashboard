@extends('layouts.dash')
<title>Dashboard | Master Data</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Master Data
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active">Level Member</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp 
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Level Member</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Direct Bonus</th>
                  <th>Sponsor Request</th>
                  <th>Bot Request</th>
                  <th>Total Saldo</th>
                  <th>Max WD</th>
                  <th>Profit Share</th>
                  <th>Priority</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp 
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->direct_bonus}}</td>
                  <td>{{$dt->sponsor_request}}</td>
                  <td>{{$dt->bot_request}}</td>
                  <td>{{$dt->total_saldo}}</td>
                  <td>{{$dt->wd_max}}</td>
                  <td>{{$dt->profit_share}}</td>
                  <td>{{$dt->priority}}</td>
                  <td><a class="btn btn-warning" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                      </a> 
                      <a href="/dashboard/dev/level/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                      </a> 
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!-- 
      Modal --> 
      @foreach ($datas as $dt)
      <div class="modal fade" id="modal-edit{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Level</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.level.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                	<input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" required="" value="{{$dt->id}}">
                  <label for="nama" class="col-sm-2 control-label">Nama Level</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Bank Persada" required="" value="{{$dt->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="sponsor_request" class="col-sm-2 control-label">Sponsor Request</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="sponsor_request" name="sponsor_request" placeholder="335" required="" value="{{$dt->sponsor_request}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="bot_request" class="col-sm-2 control-label">Bot Request</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="bot_request" placeholder="10" name="bot_request" required="" value="{{$dt->bot_request}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="direct_bonus" class="col-sm-2 control-label">Direct Bonus</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="direct_bonus" placeholder=10" name="direct_bonus" required="" value="{{$dt->direct_bonus}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="total_saldo" class="col-sm-2 control-label">Total Saldo</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="total_saldo" placeholder=10" name="total_saldo" required="" value="{{$dt->total_saldo}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="wd_max" class="col-sm-2 control-label">Max WD</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="wd_max" placeholder=10" name="wd_max" required="" value="{{$dt->wd_max}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="profit_share" class="col-sm-2 control-label">Profit Share</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="profit_share" placeholder=10" name="profit_share" required="" value="{{$dt->profit_share}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="profit_share" class="col-sm-2 control-label">Priority</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="priority" id="priority">
                      <option value="low">Low</option>
                      <option value="Medium">Medium</option>
                      <option value="high">High</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      @endforeach
      <div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Level</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.level.store') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Level</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Bank Persada" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="sponsor_request" class="col-sm-2 control-label">Sponsor Request</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="sponsor_request" name="sponsor_request" placeholder="335" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="bot_request" class="col-sm-2 control-label">Bot Request</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="bot_request" placeholder="10" name="bot_request" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="direct_bonus" class="col-sm-2 control-label">Direct Bonus</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="direct_bonus" placeholder=10" name="direct_bonus" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="total_saldo" class="col-sm-2 control-label">Total Saldo</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="total_saldo" placeholder=10" name="total_saldo" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="wd_max" class="col-sm-2 control-label">Max WD</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="wd_max" placeholder=10" name="wd_max" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="profit_share" class="col-sm-2 control-label">Profit Share</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="profit_share" placeholder=10" name="profit_share" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="profit_share" class="col-sm-2 control-label">Priority</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="priority" id="priority">
                      <option value="low">Low</option>
                      <option value="Medium">Medium</option>
                      <option value="high">High</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
@endsection
