@extends('layouts.dash')
<title>Dashboard | Manajemen User</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Manajemen User 
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Manajemen User</a></li>
        <li class="active">User Dashboard</li>
      </ol>
    </section>
     <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('gagal')){ @endphp 
      <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data yang anda masukkan sudah terdaftar, silahkan isi data yang berbeda 
      </div> @php } @endphp
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User Dashboard</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form class="form-horizontal" action="{{ route('dashboard.dev.user.create.store') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Michael Sumadi" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="myemail@email.com" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="role" class="col-sm-2 control-label">Role</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from roles");
                    @endphp
                    <select class="form-control" name="role" id="role">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
