@extends('layouts.dash')
<title>Dashboard | Master Data</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Master Data
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active">Contact</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Contact</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            @foreach ($datas as $dt)
               <form class="form-horizontal" action="{{ route('dashboard.dev.contact.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                  <label for="facebook" class="col-sm-2 control-label">Facebook</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="facebook" name="facebook" placeholder="akkunku" required="" value="{{$dt->facebook}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="twitter" class="col-sm-2 control-label">Twitter</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="twitter" name="twitter" placeholder="twitterku" required="" value="{{$dt->twitter}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="instagram" class="col-sm-2 control-label">Instagram</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="instagram" name="instagram" placeholder="instagramku" required="" value="{{$dt->instagram}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="youtube" class="col-sm-2 control-label">Youtube</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="youtubeku" required="" value="{{$dt->youtube}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Website" class="col-sm-2 control-label">Website</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="website" name="website" placeholder="websiteku" required="" value="{{$dt->website}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="telp" class="col-sm-2 control-label">Telepon</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="telp" name="telp" placeholder="08777777" required="" value="{{$dt->telp}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamatku" required="" value="{{$dt->alamat}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="email@email.com" required="" value="{{$dt->email}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Ubah</button>
              </div>
              <!-- /.box-footer -->
            </form>
            @endforeach
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
@endsection
