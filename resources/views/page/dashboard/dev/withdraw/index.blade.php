@extends('layouts.dash')
<title>Dashboard | Withdraw</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Withdraw History
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Withdraw</a></li>
        <li class="active">History</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Withdraw History</h3>
             <!--  <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Member</th>
                  <th>Bank</th>
                  <th>Nama Akun</th>
                  <th>Amount (USDT)</th>
                  <th>Amount (IDR)</th>
                  <th>Status</th>
                  <th>Waktu</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                    $usdt = 0 ;
                        $getusdt = DB::select("select * from kurs");
                            foreach ($getusdt as $keyyy ) {
                                $usdt = $keyyy->harga_beli;
                            }
                @endphp 
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->nama_user}}</td>
                  <td>{{$dt->nama_bank}}</td>
                  <td>{{$dt->nama_akun}}</td>
                  <td>$ {{$dt->amount}}</td>
                  <td>Rp. {{round($dt->amount * $usdt,1)}}</td>
                  @if($dt->status == 18)
                    <td><center><a class="btn btn-danger" >DITOLAK
                      </a></center></td>
                    @else
                  <td><center><a class="btn btn-success" >SELESAI
                    </a></center></td>
                    @endif
                  <td>{{$dt->created_at}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <!-- CK Editor -->
    <script src="{{asset('assets/AdminLTE/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })

      $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
    //bootstrap WYSIHTML5 - text editor
  })
    </script>
@endsection
