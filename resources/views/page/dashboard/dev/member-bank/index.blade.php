@extends('layouts.dash')
<title>Dashboard | Master Data</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Master Data
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active">Member Bank</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp 
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Bank Member</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Bank</th>
                  <th>Kode</th>
                  <th>Member</th>
                  <th>Nama Akun</th>
                  <th>No. Rekening</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp 
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->bank->nama_bank}}</td>
                  <td>{{$dt->bank->kode_bank}}</td>
                  <td>{{$dt->user->name}}</td>
                  <td>{{$dt->account_name}}</td>
                  <td>{{$dt->bank_rek}}</td>
                  <td><a class="btn btn-warning" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                      </a> 
                      <a href="/dashboard/dev/member-bank/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                      </a> 
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


@foreach ($datas as $dt)
<div class="modal fade" id="modal-edit{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Bank {{$dt->nama_user}}</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.member-bank.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="id" placeholder="Michael Sumadi" name="id" required="" value="{{$dt->id}}">
                  <label for="bank_id" class="col-sm-2 control-label">Bank</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from bank_kodes");
                    @endphp
                    <select class="form-control" name="bank_id" id="bank_id">
                      @foreach ($data as $dta)
                      <option value="{{$dta->id}}">{{$dta->nama_bank}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_akun" class="col-sm-2 control-label">Nama Akun</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_akun" placeholder="Michael Sumadi" name="nama_akun" required="" value="{{$dt->account_name}}">
                  </div>
                </div>
               <div class="form-group">
                  <label for="no_rek" class="col-sm-2 control-label">No. Rekening</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="no_rek" placeholder=000123000222" name="no_rek" required="" value="{{$dt->bank_rek}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
@endforeach
<div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Bank</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.member-bank.store') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="user_id" class="col-sm-2 control-label">Member</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from users");
                    @endphp
                    <!-- <select class="form-control" name="user_id" id="user_id">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select> -->
                    <select class="form-control select2" name="user_id" id="user_id" style="width: 100%;">
                      <!-- <option selected="selected">Alabama</option> -->
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select>
                  </div> 
                  
                </div>
                <div class="form-group">
                  <label for="bank_id" class="col-sm-2 control-label">Bank</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from bank_kodes");
                    @endphp
                    <select class="form-control" name="bank_id" id="bank_id">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->nama_bank}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_akun" class="col-sm-2 control-label">Nama Akun</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_akun" placeholder="Michael Sumadi" name="nama_akun" required="" >
                  </div>
                </div>
               <div class="form-group">
                  <label for="no_rek" class="col-sm-2 control-label">No. Rekening</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="no_rek" placeholder=000123000222" name="no_rek" required="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>

    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        })
    </script>
    <script>
      //Initialize Select2 Elements
      
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
 

<!-- Select2 -->
<script src="{{asset('assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

@endsection
