@extends('layouts.dash')
<title>Dashboard | Keuntungan</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Manajemen User
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Keuntungan</a></li>
        <li class="active">MT 5</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Generate Keuntungan berhasil
      </div> @php } @endphp
      @php if(Session::get('gagal')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Email sudah pernah terdaftar
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data User Dashboard</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Tanggal</th>
                  <th>Persen</th>
                  <th>Action</th>
                  <th class="col-md-1">Generate</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp
                @foreach ($data as $key => $value)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$value->tanggal}}</td>
                  <td>{{$value->persen}}%</td>

                  <td class="">
                      @if ($value->status_generate == 0)
                      <a class="btn btn-warning col-md-2" data-toggle="modal" data-target="#modal-edit{{$value->id}}"><i class="fa fa-pencil"></i></a>
                      @endif
                      <form action="{{route('dashboard.dev.keuntungan.destroy',$value->id)}}" method="POST">
                        @method('delete')
                        @csrf

                    </form>
                  </td>
                  <td>
                    @if ($value->status_generate == 0)
                    <a href="{{route('dashboard.dev.keuntungan.generate',$value->id)}}" class="btn btn-warning" type="submit" onclick="return(confirm('Generate Keuntungan Tanggal {{$value->tanggal}}?'));">Generate</a>
                    @endif

            </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

{{-- modal --}}
@foreach ($data as $key => $value)
      <div class="modal fade" id="modal-edit{{$value->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit User Dashboard</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.keuntungan.update',$value->id) }}" method="post">
                @method('PUT')
            @csrf
            <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="nama" name="tanggal" placeholder="Date" required="" value="{{$value->tanggal}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Persentase</label>
                  <div class="col-sm-10">
                    <input type="number" step="0.01" class="form-control" id="email" name="persen" placeholder="1.56" required="" value="{{$value->persen}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      @endforeach


<div class="modal fade" id="modal-tambah" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah User</h4>
        </div>
        <div class="modal-body">

         <form class="form-horizontal" action="{{ route('dashboard.dev.keuntungan.store') }}" method="post">
        @csrf
          <div class="box-body">
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Tanggal</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="nama" name="tanggal" placeholder="Date" required="" >
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-2 control-label">Persentase</label>
              <div class="col-sm-10">
                <input type="number" step="0.01" class="form-control" id="email" name="persen" placeholder="1.56" required="" >
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
          <!-- /.box-footer -->
        </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>


    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })


      function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
    </script>


@endsection
