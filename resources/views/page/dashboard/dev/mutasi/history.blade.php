@extends('layouts.dash',['title'=>'History mutasi'])
@section('script-top')
    <title>History mutasi</title>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen User
            <!-- <small>advanced tables</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Data Mutasi Moota</a></li>
            <li class="active">History</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Mutasi Moota</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Moota Id</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Balance</th>
                                    <th>Bank Id</th>
                                    <th>Bank Type</th>
                                    <th>Description</th>
                                    <th>Account Number</th>
                                </tr>
                            </thead>
                            @foreach ($datas as $item)
                                <tbody>
                                    <td>{{ $item->id_moota }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->type }}</td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ $item->balance }}</td>
                                    <td>{{ $item->bank_id }}</td>
                                    <td>{{ $item->bank_type }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>{{ $item->account_number }}</td>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{ asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}">
    </script>

    <script>
        $(function() {
            $('#table1').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true
            })
            $('#table2').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true
            })
        })
    </script>
@endsection
