@extends('layouts.dash',['title'=>'user_index'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable with default features</h3>
                        </div>
                        <!-- /.card-header -->
                        <a href="{{ route('dashboard.cashier.mutasi.index') }}"><button type="button"
                                class="btn btn-primary">Back</button></a>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>account_number</th>
                                        <th>date</th>
                                        <th>description</th>
                                        <th>amount</th>
                                        <th>type</th>
                                        <th>note</th>
                                        <th>balance</th>
                                        <th>created_at</th>
                                        <th>updated_at</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                @foreach ($datas as $item)
                                    <tbody>
                                        <td>{{ $item->account_number }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>{{ $item->note }}</td>
                                        <td>{{ $item->balance }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->updated_at }}</td>
                                        <td>
                                            <form action="{{ route('dashboard.dev.mutasi.index.usercek') }}"
                                                method="post">
                                                @csrf
                                                <input type="hidden" name="account_number"
                                                    value="{{ $item->account_number }}">
                                                <input type="hidden" name="date" value="{{ $item->date }}">
                                                <input type="hidden" name="description" value="{{ $item->description }}">
                                                <input type="hidden" name="amount" value="{{ $item->amount }}">
                                                <input type="hidden" name="type" value="{{ $item->type }}">
                                                <input type="hidden" name="note" value="{{ $item->note }}">
                                                <input type="hidden" name="balance" value="{{ $item->balance }}">
                                                <input type="hidden" name="created_at" value="{{ $item->created_at }}">
                                                <input type="hidden" name="updated_at" value="{{ $item->updated_at }}">
                                                <button type="submit" class="btn btn-success">Aprove</button>
                                            </form>
                                        </td>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
