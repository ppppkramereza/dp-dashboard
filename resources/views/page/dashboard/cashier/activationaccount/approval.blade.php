@extends('layouts.dash')
<title>Dashboard | Activation Account</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Activation Account
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Activation Account</a></li>
        <li class="active">Approval</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasilapprove')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Berhasil Approve
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Activation Account Approval</h3>
             <!--  <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                                    <tr><th>#</th>
                                        <th>Member</th>
                                        <th>Amount</th>
                                        <th>Bank</th>
                                        <th>Kurs Jual</th>
                                        <th>Kode Unik</th>
                                        <th>Status</th>
                                        <th>Batas Pembayaran</th>                                        <th>Bukti Bayar</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($datas as $item)
                                    <tbody>
                                        <td>{{$i++}}</td>
                                        <td>{{ $item->nama_user }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->nama_bank }}</td>
                                        <td>{{ $item->kurs_jual }}</td>
                                        <td>{{ $item->kode_unik }}</td>
                                        <td>{{ $item->nama_status }}</td>
                                        <td>{{ $item->batas_pembayaran }}</td>
                                        @if ($item->image_id = null)
                                            <td>Belum Upload</td>
                                        @else
                                            <td><img src="{{$item->gambar}}" width="60" ></td>
                                        @endif
                                        <td>
                                            <a href="/dashboard/cashier/activation-account/approve{{$item->id}}" class="btn btn-primary " onclick="return(confirm('Approve Akun {{$item->nama_user}}?'));"><i class="fa fa-check"></i>
                                        </td>
                                    </tbody>
                                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <!-- CK Editor -->
    <script src="{{asset('assets/AdminLTE/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })

      $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
    //bootstrap WYSIHTML5 - text editor
  })
    </script>
@endsection
