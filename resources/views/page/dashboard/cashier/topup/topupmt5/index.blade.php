@extends('layouts.dash')
<title>Dashboard | Manajemen User</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Saldo MT5
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Topup</a></li>
        <li class="active">Saldo MT5</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasilupdate')){ @endphp
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      @php if(Session::get('gagal')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Email sudah pernah terdaftar
      </div> @php } @endphp
      @php if(Session::get('berhasil')){ @endphp
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('hapus')){ @endphp
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil dihapus
      </div> @php } @endphp

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Top Up MT5</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Topup MT5</a>
                <a data-toggle="modal" data-target="#modal-tambahmodal" style="margin-left: 13px"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Modal User</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Saldo MT5</th>
                  <th>Modal User</th>
                  <th>Last Uptade</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp
                @foreach ($selectmt5 as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->name}}</td>
                  <td>{{$dt->mt5->balance}}</td>
                  <td>{{$dt->modal_user}}</td>
                  <td>{{$dt->mt5->last_update}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!--
      Modal -->


      <div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Topup MT5</h4>
            </div>
            <div class="modal-body">

             <form class="form-horizontal" action="{{ route('dashboard.cashier.topup.topupmt5.tambah') }}" method="post">
            @csrf
              <div class="box-body">

                <div class="form-group">
                  <label for="id" class="col-sm-2 control-label">Nama User</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from users where id > 5");
                    @endphp
                    <select class="form-control select2" name="id" id="id" style="width: 100%;">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>

                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                    <label for="nama" class="col-sm-2 control-label">Balance</label>
                    <div class="col-sm-10">
                      <input type="" class="form-control" id="balance" name="balance" placeholder="100" required="" >
                    </div>
                  </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
  <div class="modal fade" id="modal-tambahmodal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Modal User</h4>
            </div>
            <div class="modal-body">

             <form class="form-horizontal" action="{{ route('dashboard.cashier.topup.topupmt5.tambahmodal') }}" method="post">
            @csrf
              <div class="box-body">

                <div class="form-group">
                  <label for="id" class="col-sm-2 control-label">Nama Member</label>
                  <div class="col-sm-10" style="margin-top: 12px">
                    @php
                    $data = DB::select("select * from users");
                    @endphp
                    <select class="form-control select2" name="id" id="id" style="width: 100%;">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_1" data-toggle="tab">USD</a></li>
                      <li><a href="#tab_2" data-toggle="tab">IDR</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1">
                        <div class="form-group">
                            <label for="modal" class="col-sm-2 control-label">Modal User (USD)</label>
                            <div class="col-sm-10">
                              <input type="number" step="0.01" class="form-control" id="modal" name="modal" placeholder="100" style="margin-top: 18px">
                            </div>
                        </div>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <div class="form-group">
                            <label for="modalrupiah" class="col-sm-2 control-label">Modal User (IDR)</label>
                            <div class="col-sm-10">
                              <input type="number" class="form-control" id="modalrupiah" name="modalrupiah" placeholder="100" style="margin-top: 18px">
                            </div>
                        </div>
                      </div>

                    </div>
                    <!-- /.tab-content -->
                  </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
    <script>
        $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        })
    </script>
    <script src="{{asset('assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

@endsection
