@extends('layouts.dash')
<title>Dashboard | Withdraw</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Withdraw Approval
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Withdraw</a></li>
        <li class="active">Approval</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasilapprove')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Withdraw berhasil Approve
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
       @php if(Session::get('berhasiltolak')){ @endphp 
      <div class="alert alert-danger" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    Topup Telah ditolak
      </div> @php } @endphp   
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Withdraw</h3>
             <!--  <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Member</th>
                  <th>Bank</th>
                  <th>Nama Akun</th>
                  <th>Amount (USDT)</th>
                  <th>Amount (IDR)</th>
                  <th>Status</th>
                  <th>Waktu</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                    $usdt = 0 ;
                        $getusdt = DB::select("select * from kurs");
                            foreach ($getusdt as $keyyy ) {
                                $usdt = $keyyy->harga_beli;
                            }
                @endphp 
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->nama_user}}</td>
                  <td>{{$dt->nama_bank}}</td>
                  <td>{{$dt->nama_akun}}</td>
                  <td>$ {{$dt->amount}}</td>
                  <td>Rp. {{round($dt->amount * $usdt,1)}}</td>
                  <td>{{$dt->nama_status}}</td>
                  <td>{{$dt->created_at}}</td>
                  <td>
                  <!-- <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                      </a> 
                      <a href="" class="btn btn-danger btn-xs" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                      </a> --> 
                     <a data-toggle="modal" data-target="#modal-detail{{$dt->id}}" class="btn btn-primary " <i class="fa fa-eye"></i> Detail
                  </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

       @foreach ($datas as $dt)
      <div class="modal fade" id="modal-detail{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Topup Member</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.cashier.withdraw.approve')}}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Michael Sumadi" required="" value="{{$dt->nama_user}}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Bank</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" placeholder="myemail@email.com" required="" value="{{$dt->nama_bank}}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_akun" class="col-sm-2 control-label">Akun Bank</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_akun" name="nama_akun" placeholder="myemail@email.com" required="" value="{{$dt->nama_akun}}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nominal" class="col-sm-2 control-label">Nominal</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="nominal" name="nominal" placeholder="myemail@email.com" required="" value="Rp. {{ number_format($dt->amount,0, ',' , '.')}},-" readonly="">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="nominal" name="nominal" placeholder="myemail@email.com" required="" value="${{round($dt->amount * $usdt, 1)}}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="status" name="status" placeholder="myemail@email.com" required="" value="{{$dt->nama_status}}" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="tgl" class="col-sm-2 control-label">Waktu Withdraw</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tgl" name="tgl" placeholder="myemail@email.com" required="" value="{{$dt->created_at}}" readonly="">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="tolak" name="tolak" value="tolak" class="btn btn-danger">Tolak</button>

                <button type="submit" class="btn btn-success">Setuju</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      @endforeach

    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <!-- CK Editor -->
    <script src="{{asset('assets/AdminLTE/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })

      $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
    //bootstrap WYSIHTML5 - text editor
  })
    </script>
@endsection
