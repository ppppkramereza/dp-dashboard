<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('assets/img/logodailynew.png') }}" type="image/icon type">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/AdminLTE/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('assets/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('assets/AdminLTE/bower_components/morris.js/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('assets/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('assets/AdminLTE/bower_components/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.css">



  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('script-top')

</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <!-- <span class="logo-mini"><b>A</b>LT</span> -->
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>DAILYPIPS</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <!-- <img src="{{ asset('assets/AdminLTE/dist/img/user2-160x160.jpg')}}" class="user-image"
                                    alt="User Image"> -->
                                    <span class="hidden-xs">{{ auth()->user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->

                                <!-- Menu Body -->
                                <li class="user-body">
                <div class="row"><!--
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div> -->
                <center>
                  <a data-toggle="modal" data-target="#modal-editprofil" class="btn btn-default btn-flat">Edit Profile</a>
                  <a data-toggle="modal" data-target="#modal-editpassword" class="btn btn-default btn-flat">Edit Password</a>
                </center>
                </div>
              </li>
                                <!-- Menu Footer-->
                    <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" style="cursor: pointer" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                                            class="btn btn-danger btn-flat"><i class="fa fa-sign-out"></i> Sign Out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                         <!--    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


        <!-- Modal -->
        <div class="modal fade" id="modal-editprofil" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Profil</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.user.profil') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama </label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="id" placeholder="Michael Sumadi" name="id" required="" value="{{ auth()->user()->id }}">
                    <input type="text" class="form-control" id="nama" placeholder="Michael Sumadi" name="nama" required="" value="{{ auth()->user()->name }}">
                  </div>
                </div>
               <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" placeholder=email@email.co" name="email" required="" value="{{ auth()->user()->email }}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>



        <div class="modal fade" id="modal-editpassword" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Password</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.dev.user.password') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="id" placeholder="Masukkan Password lama" name="id" required="" value="{{ auth()->user()->id }}">
                  <label for="passwordlama" class="col-sm-2 control-label">Password Lama </label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="passwordlama" placeholder="Masukkan Password lama" name="passwordlama" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="passwordbaru" class="col-sm-2 control-label">Password Baru </label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="passwordbaru" placeholder="Masukkan Password Baru kamu" name="passwordbaru" required="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>

      <div class="modal fade" id="modal-profilberhasil" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <center><h4>Profil Berhasil diubah</h4></center>
                <center><img src="{{ asset('assets/img/ceklislingk.jpg') }}"></center>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      <div class="modal fade" id="modal-passwordberhasil" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <center><h4>Password Berhasil diubah</h4></center>
                <center><img src="{{ asset('assets/img/ceklislingk.jpg') }}"></center>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
      <div class="modal fade" id="modal-passwordgagal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <center><h4>Password gagal diubah, password lama kamu tidak sesuai</h4></center>
                <center><img src="{{ asset('assets/img/silang.jpg') }}"></center>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->

                <!-- search form -->
                <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    @role('Dev')
                            @php if(Session::get('menu') == 'home'){ @endphp
                        <li class="active">
                            @php } if(Session::get('menu') != 'home'){ @endphp
                        <li>
                            @php } @endphp
                            <a href="/home">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                            @php if(Session::get('menu') == 'masterdata'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'masterdata'){ @endphp
                        <li class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-bars"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                            @php if(Session::get('sub') == 'bank'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'bank'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/bank"><i class="fa fa-circle-o"></i> Bank</a>
                                </li>

                            @php if(Session::get('sub') == 'levelmember'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'levelmember'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/level"><i class="fa fa-circle-o"></i> Level Member</a></li>

                            @php if(Session::get('sub') == 'robot'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'robot'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/robot"><i class="fa fa-circle-o"></i>Robot</a></li>

                            @php if(Session::get('sub') == 'kurs'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'kurs'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/kurs"><i class="fa fa-circle-o"></i>Kurs</a></li>

                            @php if(Session::get('sub') == 'memberbank'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'memberbank'){ @endphp
                                <li>
                            @php } @endphp
                                <a href="/dashboard/dev/member-bank"><i class="fa fa-circle-o"></i>Member Bank</a></li>

                            @php if(Session::get('sub') == 'kontak'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'kontak'){ @endphp
                                <li>
                            @php } @endphp<a href="/dashboard/dev/contact"><i class="fa fa-circle-o"></i>Contact</a></li>
                            </ul>
                        </li>

                            @php if(Session::get('menu') == 'manajemenuser'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'manajemenuser'){ @endphp
                        <li class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-user-o"></i> <span>Manejemen User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                            @php if(Session::get('sub') == 'userdashboard'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'userdashboard'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/user"><i class="fa fa-circle-o"></i> User Dashboard</a></li>


                            @php if(Session::get('sub') == 'userapps'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'userapps'){ @endphp
                                <li>
                            @php } @endphp<a href="/dashboard/dev/member"><i class="fa fa-circle-o"></i> User Apps</a></li>


                            @php if(Session::get('sub') == 'akunmt5'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'akunmt5'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="/dashboard/dev/akunmt5"><i class="fa fa-circle-o"></i> Akun MT5</a></li>
                            </ul>
                        </li>
                            @php if(Session::get('menu') == 'mutasi'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'mutasi'){ @endphp
                        <li class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-exchange"></i> <span>Muttation</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                @php if(Session::get('sub') == 'history'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'history'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.mutasi.index.history') }}"><i
                                            class="fa fa-circle-o"></i>
                                        History</a></li>

                            @php if(Session::get('sub') == 'check'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'check'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.mutasi.index') }}"><i class="fa fa-circle-o"></i>
                                Check</a></li>
                            </ul>
                        </li>

                            @php if(Session::get('menu') == 'activation'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'activation'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-check"></i> <span>Activation Account</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                            @php if(Session::get('sub') == 'activated'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'activated'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.activation-account.index') }}"><i class="fa fa-circle-o"></i>
                                        Activated</a></li>

                            @php if(Session::get('sub') == 'approval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'approval'){ @endphp
                                <li>
                            @php } @endphp
                                    <a href="{{ route('dashboard.dev.activation-account.approval') }}">
                                    <i class="fa fa-circle-o">
                                    </i>
                                        Approval</a></li>
                            </ul>
                        </li>


                            @php if(Session::get('menu') == 'topup'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'topup'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Top Up</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                            @php if(Session::get('sub') == 'topupindex'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupindex'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.topup.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        History</a></li>

                            @php if(Session::get('sub') == 'topupapproval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupapproval'){ @endphp
                                <li>
                            @php } @endphp<a href="{{ route('dashboard.dev.topup.approval') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Approval</a></li>

                            @php if(Session::get('sub') == 'topupmt5'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupmt5'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.topupmt5.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Mt5</a></li>
                            </ul>
                        </li>

                         @php if(Session::get('menu') == 'wd'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'wd'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Withdraw</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                            @php if(Session::get('sub') == 'wdindex'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'wdindex'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.dev.withdraw.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        History</a></li>
                            @php if(Session::get('sub') == 'wdapproval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'wdapproval'){ @endphp
                                <li>
                            @php } @endphp

                                    <a href="{{ route('dashboard.dev.withdraw.approval') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Approval</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Keuntungan</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('dashboard.dev.keuntungan.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        MT5</a></li>
                                <li><a href=""><i
                                            class="fa fa-circle-o"></i>
                                        Bot</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route('dashboard.dev.version.index')}}">
                                <i class="fa fa-dollar"></i> <span>Version Apps</span>
                            </a>
                        </li>
                        <li class=" {{(request()->is('dashboard/dev/bypassrobot')) ? 'active' : null }}">
                            <a href="{{route('dashboard.dev.bypassrobot.index')}}">
                                <i class="fa fa-dollar"></i> <span >Bypass Robot </span>
                            </a>
                        </li>
                            @php if(Session::get('menu') == 'network'){ @endphp
                        <li class="active">
                            @php } if(Session::get('menu') != 'network'){ @endphp
                        <li>
                            @php } @endphp
                            <a href="{{route('dashboard.dev.network.index')}}">
                                <i class="fa fa-code-fork"></i> <span>Network</span>
                            </a>
                        </li>
                        <li class=" {{(request()->is('dashboard/dev/data-trc')) ? 'active' : null }}">
                            <a href="{{route('dashboard.dev.TRC20.index')}}">
                                <i class="fa fa-diamond"></i> <span >TRC20 </span>
                            </a>
                        </li>
                    @endrole
                    @role('Owner')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Customer</span>
                                <!-- <span class="pull-right-container">
                                                                                                                                                                                                                                                                                <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                                                                                                            </span> -->
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-circle-o"></i> User</a></li>
                            </ul>
                        </li>
                    @endrole
                    @role('Broker')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Customer</span>
                                <!-- <span class="pull-right-container">
                                                                                                                                                                                                                                                                                <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                                                                                                            </span> -->
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-circle-o"></i> User</a></li>
                            </ul>
                        </li>
                    @endrole
                    @role('Directure')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Customer</span>
                                <!-- <span class="pull-right-container">
                                                                                                                                                                                                                                                                                <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                                                                                                            </span> -->
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-circle-o"></i> User</a></li>
                            </ul>
                        </li>
                    @endrole
                    @role('Manager')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Customer</span>
                                <!-- <span class="pull-right-container">
                                                                                                                                                                                                                                                                                <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                                                                                                            </span> -->
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=""><i class="fa fa-circle-o"></i> User</a></li>
                            </ul>
                        </li>
                    @endrole
                    @role('Cashier')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Cashier</span>
                                <!-- <span class="pull-right-container">
                                                                                                                                                                                                                                                                                <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                                                                                                            </span> -->
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user-o"></i> <span>Manajemen User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('dashboard.cashier.masterdata.user.index')}}"><i class="fa fa-circle-o"></i> User Apps</a></li>
                                <li><a href="{{route('dashboard.cashier.manajemenuser.akunmt5.index')}}"><i class="fa fa-circle-o"></i> Akun MT5</a></li>
                            </ul>
                        </li>
                        @php if(Session::get('menu') == 'activation'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'activation'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-check"></i> <span>Activation Account</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                            @php if(Session::get('sub') == 'activated'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'activated'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.cashier.activation-account.index') }}"><i class="fa fa-circle-o"></i>
                                        Activated</a></li>

                            @php if(Session::get('sub') == 'approval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'approval'){ @endphp
                                <li>
                            @php } @endphp
                                    <a href="{{ route('dashboard.cashier.activation-account.approval') }}">
                                    <i class="fa fa-circle-o">
                                    </i>
                                        Approval</a></li>
                            </ul>
                        </li>

                        @php if(Session::get('menu') == 'topup'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'topup'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Top Up</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                            @php if(Session::get('sub') == 'topupindex'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupindex'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.cashier.topup.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        History</a></li>

                            @php if(Session::get('sub') == 'topupapproval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupapproval'){ @endphp
                                <li>
                            @php } @endphp<a href="{{ route('dashboard.cashier.topup.approval') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Approval</a></li>

                            @php if(Session::get('sub') == 'topupmt5'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'topupmt5'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.cashier.topup.topupmt5.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Mt5</a></li>
                            </ul>
                        </li>

                         @php if(Session::get('menu') == 'wd'){ @endphp
                        <li class="treeview active">
                            @php } if(Session::get('menu') != 'wd'){ @endphp
                        <li  class="treeview">
                            @php } @endphp
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Withdraw</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                            @php if(Session::get('sub') == 'wdindex'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'wdindex'){ @endphp
                                <li>
                            @php } @endphp
                            <a href="{{ route('dashboard.cashier.withdraw.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        History</a></li>
                            @php if(Session::get('sub') == 'wdapproval'){ @endphp
                                <li class="active">
                            @php } if(Session::get('sub') != 'wdapproval'){ @endphp
                                <li>
                            @php } @endphp

                                    <a href="{{ route('dashboard.cashier.withdraw.approval') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Approval</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Mutasi</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ route('dashboard.cashier.mutasi.index.history') }}"><i
                                            class="fa fa-circle-o"></i>
                                        history</a></li>
                                <li><a href="{{ route('dashboard.cashier.mutasi.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Check</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money"></i> <span>Keuntungan</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ route('dashboard.cashier.keuntungan.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Mt5</a></li>

                            </ul>
                        </li>
                    @endrole
                    @role('Customer')
                        <li class="">
                            <a href="{{ route('dashboard.') }}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard - Customer</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Robot</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('dashboard.customer.list-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        List Robot</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Pembelian Robot</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="{{ route('dashboard.customer.profile.index') }}">
                                <i class="fa fa-profile"></i> <span>Profile</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('dashboard.customer.profile.index') }}">
                                <i class="fa fa-profile"></i> <span>Network</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Poin</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('dashboard.customer.list-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Affiliate</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Overriding</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Profit Share</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Spread</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Reward</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-o"></i> <span>Exchange</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('dashboard.customer.list-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Buy</a></li>
                                <li><a href="{{ route('dashboard.customer.beli-robot.index') }}"><i
                                            class="fa fa-circle-o"></i>
                                        Sell</a></li>
                            </ul>
                        </li>
                    @endrole
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">

            </div>
            <strong>Copyright &copy; <a href="">AdminLTE</a>.</strong> All rights
            reserved by PT. Bangkit Digital Indonesia
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="display: none;">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Recent Activity</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                    <p>Will be 23 on April 24th</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-user bg-yellow"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                    <p>New phone +1(800)555-1234</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                    <p>nora@example.com</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                    <p>Execution time 5 seconds</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                                    <span class="label label-danger pull-right">70%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Update Resume
                                    <span class="label label-success pull-right">95%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Laravel Integration
                                    <span class="label label-warning pull-right">50%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Back End Framework
                                    <span class="label label-primary pull-right">68%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
                <div class="tab-pane" id="control-sidebar-settings-tab">
                    <form method="post">
                        <h3 class="control-sidebar-heading">General Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Report panel usage
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Some information about this general settings option
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Allow mail redirect
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Other sets of options are available
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Expose author name in posts
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Allow the user to show his name in blog posts
                            </p>
                        </div>
                        <!-- /.form-group -->

                        <h3 class="control-sidebar-heading">Chat Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Show me as online
                                <input type="checkbox" class="pull-right" checked>
                            </label>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Turn off notifications
                                <input type="checkbox" class="pull-right">
                            </label>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Delete chat history
                                <a href="javascript:void(0)" class="text-red pull-right"><i
                                        class="fa fa-trash-o"></i></a>
                            </label>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="{{ asset('assets/AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('assets/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('assets/AdminLTE/bower_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/AdminLTE/bower_components/morris.js/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('assets/AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('assets/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('assets/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('assets/AdminLTE/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script
        src="{{ asset('assets/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
    </script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('assets/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('assets/AdminLTE/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/AdminLTE/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('assets/AdminLTE/dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('assets/AdminLTE/dist/js/demo.js') }}"></script>
    <!-- page script -->


    @php if(Session::get('updateprofil')){ @endphp
    <script>
        $(document).ready(function(){
        $("#modal-profilberhasil").modal('show');
    });
    </script>
    @php } @endphp

    @php if(Session::get('updatepassword')){ @endphp
    <script>
        $(document).ready(function(){
        $("#modal-passwordberhasil").modal('show');
    });
    </script>
    @php } @endphp

    @php if(Session::get('updatepasswordgagal')){ @endphp
    <script>
        $(document).ready(function(){
        $("#modal-passwordgagal").modal('show');
    });
    </script>
    @php } @endphp

    @yield('scripts')
    @yield('script-bottom')

</body>

</html>
