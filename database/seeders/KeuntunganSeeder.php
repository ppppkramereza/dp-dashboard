<?php

namespace Database\Seeders;

use App\Models\Keuntungan;
use Illuminate\Database\Seeder;

class KeuntunganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Keuntungan::create([
            'tanggal' => '2022-01-3',
            'persen' => '2.79',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-4',
            'persen' => '0.51',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-5',
            'persen' => '0.45',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-6',
            'persen' => '-2.96',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-7',
            'persen' => '1.59',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-10',
            'persen' => '1.79',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-11',
            'persen' => '0.4',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-12',
            'persen' => '1.8',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-17',
            'persen' => '1.07',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-18',
            'persen' => '3.17',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-19',
            'persen' => '3.01',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-20',
            'persen' => '1.16',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-21',
            'persen' => '0.65',
            'status' => 'Bot ON'
        ]);
        Keuntungan::create([
            'tanggal' => '2022-01-24',
            'persen' => '1.6',
            'status' => 'Bot ON'
        ]);
    }
}
