<?php

namespace Database\Seeders;

use App\Models\Bankkode;
use Illuminate\Database\Seeder;

class BankkodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bankkode::create([
            'nama_bank' => 'BANK BRI',
            'kode_bank' => '002',
            'image_id' => 'level1',
        ]);
        Bankkode::create([
            'nama_bank' => 'BANK BCA',
            'kode_bank' => '014',
            'image_id' => 'level1',
        ]);
        Bankkode::create([
            'nama_bank' => 'BANK MANDIRI',
            'kode_bank' => '008',
            'image_id' => 'level1',
        ]);
        Bankkode::create([
            'nama_bank' => 'BANK BNI',
            'kode_bank' => '009',
            'image_id' => 'level1',
        ]);
    }
}
