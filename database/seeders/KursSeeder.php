<?php

namespace Database\Seeders;

use App\Models\Kurs;
use Illuminate\Database\Seeder;

class KursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kurs::create([
            'nama' => 'USDT',
            'harga_jual' => '15000',
            'harga_beli' => '15000',
        ]);
    }
}
