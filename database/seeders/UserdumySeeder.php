<?php

namespace Database\Seeders;

use App\Models\Downline;
use App\Models\Upline;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Wallet;

class UserdumySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Customer',
            'email' => Str::random(8),
            'country_code' => '62',
            'phone' => Str::random(8),
            'refferal' => Str::random(8) . Carbon::now()->timestamp . Str::random(8),
            'status' => '1',
            'level_id' => '6',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Upline::create([
            'user_id' => $user->id,
            'upline_id' => 1
        ]);
        Downline::create([
            'user_id' => 1,
            'downline_id' => $user->id
        ]);
        $user->assignRole('Customer');

        //
        $user2 = User::create([
            'name' => 'Customer',
            'email' => Str::random(8),
            'country_code' => '62',
            'phone' => Str::random(8),
            'refferal' => Str::random(8) . Carbon::now()->timestamp . Str::random(8),
            'status' => '1',
            'level_id' => '2',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);
        Wallet::create([
            'user_id' => $user2->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Upline::create([
            'user_id' => $user2->id,
            'upline_id' => $user->id,
        ]);
        Downline::create([
            'user_id' => $user->id,
            'downline_id' => $user2->id
        ]);
        $user2->assignRole('Customer');

        //
        $user3 = User::create([
            'name' => 'Customer',
            'email' => Str::random(8),
            'country_code' => '62',
            'phone' => Str::random(8),
            'refferal' => Str::random(8) . Carbon::now()->timestamp . Str::random(8),
            'status' => '1',
            'level_id' => '1',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);
        Wallet::create([
            'user_id' => $user3->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Upline::create([
            'user_id' => $user3->id,
            'upline_id' => $user2->id,
        ]);
        Downline::create([
            'user_id' => $user2->id,
            'downline_id' => $user3->id
        ]);
        $user3->assignRole('Customer');
    }
}
