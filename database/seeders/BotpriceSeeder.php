<?php

namespace Database\Seeders;

use App\Models\Botprice;
use Illuminate\Database\Seeder;

class BotpriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Botprice::create([
            'name' => 'Expert Advisor',
            'price' => '100',
            'description' => '
            <h1>AFBOT</h1>
            <br>
            <p>Solusi Bot Trading untuk mendapatkan pasive income </p>'
        ]);
    }
}
