<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            StatusSeeder::class,
            RolesSeeder::class,
            LevelsSeeder::class,
            KursSeeder::class,
            UsersSeeder::class,
            HistorytypeSeeder::class,
            PhoneCountrieSeeder::class,
            BotpriceSeeder::class,
            MetodepembayaranSeeder::class,
            TabelbankSeeder::class,
            BankkodeSeeder::class,
        ]);
    }
}
