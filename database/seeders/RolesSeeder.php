<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'name' => 'Dev',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Owner',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Broker',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Founder',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Manager',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Cashier',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Customer',
            'guard_name' => 'web',
        ]);
    }
}
