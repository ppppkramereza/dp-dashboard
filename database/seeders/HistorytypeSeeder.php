<?php

namespace Database\Seeders;

use App\Models\Historytype;
use Illuminate\Database\Seeder;

class HistorytypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Historytype::create([
            'name' => 'Bonus Refrerral',
        ]);
        Historytype::create([
            'name' => 'Bonus Profit',
        ]);
        Historytype::create([
            'name' => 'Top-UP',
        ]);
        Historytype::create([
            'name' => 'Withdraw',
        ]);
        Historytype::create([
            'name' => 'Bonus Cendol',
        ]);
        Historytype::create([
            'name' => 'Fee Withdraw',
        ]);
    }
}
