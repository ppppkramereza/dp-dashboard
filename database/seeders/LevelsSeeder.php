<?php

namespace Database\Seeders;

use App\Models\Images;
use App\Models\Levels;
use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //image
        $url = 'https://larapiker.updigitaltransmilenial.co.id/';
        Images::create([
            'id' => 'level1',
            'path' => 'level',
            'image' => '1.png',
            'image_url' => $url . 'level/1.png',

        ]);
        Images::create([
            'id' => 'level2',
            'path' => 'level',
            'image' => '2.png',
            'image_url' => $url . 'level/2.png',

        ]);
        Images::create([
            'id' => 'level3',
            'path' => 'level',
            'image' => '3.png',
            'image_url' => $url . 'level/3.png',

        ]);
        Images::create([
            'id' => 'level4',
            'path' => 'level',
            'image' => '4.png',
            'image_url' => $url . 'level/4.png',

        ]);
        Images::create([
            'id' => 'level5',
            'path' => 'level',
            'image' => '5.png',
            'image_url' => $url . 'level/5.png',

        ]);
        Images::create([
            'id' => 'level6',
            'path' => 'level',
            'image' => '6.png',
            'image_url' => $url . 'level/6.png',

        ]);
        Images::create([
            'id' => 'level7',
            'path' => 'level',
            'image' => '7.png',
            'image_url' => $url . 'level/7.png',

        ]);


        //user

        Levels::create([
            'name' => 'Classic',
            'direct_bonus' => '20',
            'sponsor_request' => '0',
            'bot_request' => '0',
            'total_saldo' => '0',
            'wd_max' => '10',
            'profit_share' => '10',
            'priority' => 'low',
            'image_id' => 'level1',
        ]);
        Levels::create([
            'name' => 'Ruby',
            'direct_bonus' => '30',
            'sponsor_request' => '3',
            'bot_request' => '20',
            'total_saldo' => '100',
            'wd_max' => '100',
            'profit_share' => '10',
            'priority' => 'low',
            'image_id' => 'level2',
        ]);
        Levels::create([
            'name' => 'Saphier',
            'direct_bonus' => '40',
            'sponsor_request' => '5',
            'bot_request' => '100',
            'total_saldo' => '500',
            'wd_max' => '500',
            'profit_share' => '10',
            'priority' => 'low',
            'image_id' => 'level3',
        ]);
        Levels::create([
            'name' => 'Emerald',
            'direct_bonus' => '50',
            'sponsor_request' => '8',
            'bot_request' => '300',
            'total_saldo' => '1000',
            'wd_max' => '1000',
            'profit_share' => '20',
            'priority' => 'low',
            'image_id' => 'level4',
        ]);
        Levels::create([
            'name' => 'Diamond',
            'direct_bonus' => '55',
            'sponsor_request' => '12',
            'bot_request' => '800',
            'total_saldo' => '2500',
            'wd_max' => '2500',
            'profit_share' => '20',
            'priority' => 'low',
            'image_id' => 'level5',
        ]);
        Levels::create([
            'name' => 'Black Pearl',
            'direct_bonus' => '60',
            'sponsor_request' => '20',
            'bot_request' => '1500',
            'total_saldo' => '5000',
            'wd_max' => '5000',
            'profit_share' => '30',
            'priority' => 'low',
            'image_id' => 'level6',
        ]);
        Levels::create([
            'name' => 'Owner',
            'direct_bonus' => '0',
            'sponsor_request' => '0',
            'bot_request' => '0',
            'total_saldo' => '0',
            'wd_max' => '0',
            'profit_share' => '0',
            'priority' => 'High',
            'image_id' => 'level7',
        ]);
    }
}
