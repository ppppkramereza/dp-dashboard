<?php

namespace Database\Seeders;

use App\Models\Tabelbank;
use Illuminate\Database\Seeder;

class TabelbankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tabelbank::create([
            'bank_name' => 'MANDIRI',
            'bank_code' => '008',
            'account_name' => 'PT Bangkit Digital Indonesia',
            'account_number' => '1420500121008',
        ]);
        Tabelbank::create([
            'bank_name' => 'USDT - TRC20',
            'bank_code' => '000',
            'account_name' => 'USDT - TRC20',
            'account_number' => 'TMPvk6QpBcgeXkQbDTGfXt9Y3rS2pS6NTj',
        ]);
    }
}
