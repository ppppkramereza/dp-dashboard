<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletmt5HistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walletmt5_historys', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('wallet_id')->references('id')->on('walletmt5s');
            $table->foreignId('hystory_type_id')->references('id')->on('history_types');
            $table->time('time');
            $table->string('note');
            $table->double('amount', 50, 2);
            $table->foreignId('status')->references('id')->on('status');
            $table->string('token');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walletmt5_history');
    }
}
