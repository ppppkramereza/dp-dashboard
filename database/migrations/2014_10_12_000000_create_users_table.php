<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('country_code');
            $table->string('phone')->unique();
            $table->string('refferal')->unique();
            $table->foreignid('status')->references('id')->on('status');
            $table->foreignid('level_id')->references('id')->on('levels');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('total_robot');
            $table->rememberToken();
            $table->string('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images');
            $table->double('modal_user', 50, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
