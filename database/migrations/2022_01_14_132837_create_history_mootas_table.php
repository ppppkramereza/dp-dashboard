<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMootasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_mootas', function (Blueprint $table) {
            $table->id();
            $table->string('id_moota');
            $table->string('date');
            $table->string('type');
            $table->double('amount', 50, 2);
            $table->double('balance', 50, 2);
            $table->string('bank_id');
            $table->string('bank_type');
            $table->string('description');
            $table->bigInteger('account_number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_mootas');
    }
}
