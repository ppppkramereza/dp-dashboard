<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTacsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tacs', function (Blueprint $table) {
            $table->id();
            $table->string('kode_produksi')->unique();
            $table->string('tac_1')->unique();
            $table->string('tac_2')->unique();
            $table->string('tac_3')->unique();
            $table->string('tac_4')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tacs');
    }
}
