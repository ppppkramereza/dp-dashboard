<?php

namespace App\Helper;

use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Images;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\EncapsulatedApiResponder;
use App\Models\Botprice;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Storage;

class ImageHelper
{

    public static function image($folder, $image, $id)
    {

        // $data = Images::create([
        //     'path' => $folder,
        //     'image' => $image,
        // ]);

        $data = new Images();
        $data->path = $folder;
        if ($data) {
            $idimage = $id;
            $fileName = $id . '.' . $image->getClientOriginalExtension();
            $filePath = $image->storeAs($folder, $fileName, 'public');
            $data->id = $idimage;
            $data->image = $fileName;
            $data->image_url = url('storage' . '/' . $folder . '/' . $fileName);
        }
        $data->save();

        return $data;


        // dd($idimage);
        // if ($folder == 'robot') {
        //     Botprice::where('id', 1)->update([
        //         'image_id' => $idimage,
        //     ]);
        // }
        // if ($folder == 'user') {
        //     User::where('id', auth()->user()->id)->update([
        //         'image_id' => $idimage,
        //     ]);
        // }
        // if ($folder == 'bank') {
        //     User::where('id', auth()->user()->id)->update([
        //         'image_id' => $idimage,
        //     ]);
        // }
    }
}
