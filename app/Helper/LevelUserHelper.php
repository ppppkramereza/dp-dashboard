<?php

namespace App\Helper;

use App\Models\Downline;
use App\Models\Levels;
use App\Models\Wallet;
use App\Models\Wallethistory;
use App\Models\User;
use App\Models\Upline;
use App\Models\Walletmt5;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class LevelUserHelper
{

    public static function ceklevel($id)
    {
        $level = Levels::get();
        $datauser = User::where('id', $id)->first();
        $walletmt5  = Walletmt5::where('user_id', $id)->first();
        $robotdownline = Downline::with(['downlinebot'])->where('user_id', $id)->count();

        $indexs = 5;


        do {
            if ($level[$indexs]->bot_request <= $datauser->total_robot && $level[$indexs]->total_saldo <= $walletmt5->balance && $level[$indexs]->sponsor_request <= $robotdownline) {
                User::where('id', $id)->update([
                    'level_id' => $indexs + 1,
                    // 'level_id' => 3,
                    'two_factor_secret' => 'total robot: ' . $datauser->total_robot . 'total saldo: ' . $walletmt5->balance . 'total dowbline: ' . $robotdownline,
                    'two_factor_recovery_codes' => 'hohoho' . $indexs,
                ]);
            }

            $indexs--;
        } while ($indexs > 0);
    }
}
