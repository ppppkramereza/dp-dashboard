<?php

namespace App\Jobs;

use App\Models\Upline;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Wallethistory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class ReferralJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($id)
    {
        $user = User::find($id);
        $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();
        dd('hehehe');
        if ($upline->upline_id == 1) {
            referral($upline, 100, $user);
        }
    }

    public function referral($id, $nominal, $user)
    {
        $balance = Wallet::where('user_id', $id)->first();
        Wallet::where('user_id', $id)->update([
            'balance' => $balance->balance + $nominal,
        ]);
        Wallethistory::create([
            'user_id' => $balance->upline_id,
            'wallet_id' => $balance->id,
            'hystory_type_id' => 1,
            'time' => Carbon::now(),
            'note' => 'Bonus Referral dari' . $user->id . ':' . $user->name,
            'amount' => 100,
            'token' => Str::random(60),
        ]);
    }
}
