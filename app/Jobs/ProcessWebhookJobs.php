<?php

namespace App\Jobs;

use App\Models\Activationaccount;
use App\Models\Downline;
use App\Models\Historymoota;
use App\Models\Tabelbank;
use App\Models\Upline;
use App\Models\Webhook;
use Spatie\WebhookClient\Jobs\ProcessWebhookJob;

use function App\Helper\ReferralHelper\cek;

class ProcessWebhookJobs extends ProcessWebhookJob
{
    public function handle()
    { // contains an instance of `WebhookCall`

        $data = $this->webhookCall;
        // logger($data['payload']);
        Historymoota::create([
            'id_moota' => $data['payload'][0]['mutation_id'],
            'date' => $data['payload'][0]['date'],
            'type' => $data['payload'][0]['type'],
            'amount' => $data['payload'][0]['amount'],
            'balance' => $data['payload'][0]['balance'],
            'bank_id' => $data['payload'][0]['bank_id'],
            // 'bank_type' => $data['payload'][0]['bank'],
            'bank_type' => 'Member',
            'description' => $data['payload'][0]['description'],
            'account_number' => $data['payload'][0]['account_number'],
        ]);

        // $idbank = Tabelbank::where('bank_name', $data['payload'][0]['bank_type'])->pluck('id');

        // $Activationaccount = Activationaccount::where('status', 4)->where('amount', $data['payload'][0]['amount'])->where('bank_id', $idbank)->first();
        // Activationaccount::where('status', 4)->where('amount', $data['payload'][0]['amount'])->where('bank_id', $idbank)->update([
        //     'status' => 5,
        // ]);

        // if ($Activationaccount) {
        //     cek($Activationaccount->user_id);
        // }
        // logger($Activationaccount->user_id);
    }
}
