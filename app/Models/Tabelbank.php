<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tabelbank extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'tabel_banks';
	
	protected $fillable = [
 		'bank_name',
        'bank_code',
        'account_name',
        'account_number',
	];

 
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
