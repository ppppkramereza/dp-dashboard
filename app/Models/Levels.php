<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Levels extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'levels';

    protected $fillable = [
		'name',
	    'sponsor_request',
	    'bot_request',
	    'direct_bonus',
	    'total_saldo',
	    'wd_max',
	    'profit_share',
	    'priority',
	];
    public function leveluser()
    {
        return $this->hasMany(User::class, 'level_id', 'id');
    }
}
