<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Downline extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'downlines';

    protected $fillable = [
        'user_id',
        'downline_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function downline()
    {
        return $this->belongsTo(User::class, 'downline_id', 'id');
    }

    public function downlinebot()
    {
        return $this->belongsTo(Activationaccount::class, 'downline_id', 'user_id');
    }
}
