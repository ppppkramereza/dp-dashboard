<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bankkode extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'bank_kodes';

    protected $fillable = [
        'kode_bank',
        'nama_bank',
        'image_id',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function image()
    {
        return $this->belongsTo(Images::class, 'image_id', 'id');
    }
}
