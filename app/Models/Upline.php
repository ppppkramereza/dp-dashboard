<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upline extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'uplines';

    protected $fillable = [
        'user_id',
        'upline_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function upline()
    {
        return $this->belongsTo(User::class, 'upline_id', 'id');
    }
}
