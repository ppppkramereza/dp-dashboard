<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topup extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'topups';

    protected $fillable = [
        'user_id',
        'bank_id',
        'amount',
        'unik',
        'status',
        'image_id',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status', 'id');
    }

    public function bank()
    {
        return $this->belongsTo(Tabelbank::class, 'bank_id', 'id');
    }
}
