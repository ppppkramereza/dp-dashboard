<?php

namespace App\Http\Controllers\Customer\api\v1\bank;

use App\Http\Controllers\Controller;
use App\Models\Tabelbank;
use Illuminate\Http\Request;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Metodepembayaran;
use App\Helper\ImageHelper;
use App\Models\Bankkode;
use App\Models\Bankuser;
use App\Models\Images;
use Illuminate\Auth\Events\Validated;

class BankController extends ApiController
{
    public function listbank()
    {
        $data = Tabelbank::get();
        foreach ($data as $key => $value) {
            if ($value->image_id) {
                $data[$key]['image_url'] = Images::where('id', $value->image_id)->first()->image_url;
            }
        }
        return ApiController::success('list-Bank', $data);
    }

    public function metodepembayaran()
    {
        $data = Metodepembayaran::get();
        return ApiController::success('List-Metode Pembayaran', $data);
    }

    public function getbankkode()
    {
        $id = auth()->user()->id;
        $data = Bankkode::get();
        foreach ($data as $key => $value) {
            if ($value->image_id) {
                $data[$key]['image_url'] = Images::where('id', $value->image_id)->first()->image_url;
            }
        }


        return ApiController::success('Berhasil get bank kode!!', $data);
    }

    public function getbankuser()
    {
        $id = auth()->user()->id;
        $data = Bankuser::with(['bank'])->where('user_id', $id)->get();
        foreach ($data as $key => $value) {
            if ($value->bank->image_id) {
                $data[$key]->bank['image_url'] = Images::where('id', $value->bank->image_id)->first()->image_url;
            }
        }


        return ApiController::success('Berhasil get bank user!!', $data);
    }

    public function addbankuser(Request $request)
    {

        $id = auth()->user()->id;
        $request->validate([]);

        $data = Bankuser::create([
            'bank_id' => $request->input('bank_id'),
            'account_name' => $request->input('account_name'),
            'bank_rek' => $request->input('bank_rek'),
            'user_id' => $id,
        ]);

        $data->bank = Bankkode::where('id', $data->bank_id)->first();
        return ApiController::success('berhasil memasukkan rekening bank anda!!', $data);
    }

    public function deletebankuser(Request $request)
    {

        $id = auth()->user()->id;
        $request->validate(['id' => 'required']);

        $data = Bankuser::find($request->input('id'));
        $data->delete();
        return ApiController::success('berhasil menghapus rekening bank anda!!', $data);
    }
    public function updatebankuser(Request $request)
    {

        $id = auth()->user()->id;
        $request->validate([
            'id' => 'required',
            'bank_id' => 'required',
            'account_name' => 'required',
            'bank_rek' => 'required',
        ]);

        $data = Bankuser::find($request->input('id'));
        $data->bank_id = $request->input('bank_id');
        $data->account_name = $request->input('account_name');
        $data->bank_rek = $request->input('bank_rek');
        $data->update();
        return ApiController::success('berhasil menghapus rekening bank anda!!', $data);
    }
}
