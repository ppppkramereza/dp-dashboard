<?php

namespace App\Http\Controllers\Customer\api\v1\mt5;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Accountmt5;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class mt5Controller extends ApiController
{
    public function get()
    {
        $id = auth()->user()->id;
        $data = Accountmt5::where('user_id', $id)->first();

        if (!$data) {
            return ApiController::success('Data masih belum ada', $data);
        }
        return ApiController::success('Berhasil get data akun MT5', $data);
    }

    public function update(Request $request)
    {
        $id = auth()->user()->id;
        $data = Accountmt5::where('user_id', $id)->update([
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'server' => $request->input('server'),
            'ip_server' => $request->input('ip_server'),
            'password_investor' => Hash::make($request->input('password_investor')),
        ]);
        if (!$data) {
            return ApiController::success('Data masih belum ada', $data);
        }

        return ApiController::success('Berhasil get data akun MT5', $data);
    }
}
