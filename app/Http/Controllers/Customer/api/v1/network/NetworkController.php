<?php

namespace App\Http\Controllers\Customer\api\v1\network;

use App\Http\Controllers\Controller;
use App\Models\Upline;
use Illuminate\Http\Request;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Downline;

class NetworkController extends ApiController
{

    public function upline()
    {
        $id = auth()->user()->id;
        $data = Upline::with(['user', 'upline'])->where('user_id', $id)->first();
        return ApiController::success('Data Upline', $data);
    }
    public function downline()
    {
        $id = auth()->user()->id;
        $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();
        $data->total_downline =  Downline::with(['user', 'downline'])->where('user_id', $id)->count();
        return ApiController::success('Data Downline', $data);
    }
    //dipakek
    public function downlineid(Request $request)
    {
        $id = $request->input('user_id');
        $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();
        $dat =  Downline::with(['downline'])->where('user_id', $id)->count();
        foreach ($data as $key => $value) {
            $value->total_downline =  Downline::with(['downline'])->where('user_id', $value->downline_id)->count();
            // $value->total_downline =  $value->user_id;
        }
        return ApiController::success('Data Downlines', $data);
    }
    public function directsponsor()
    {
        $id = auth()->user()->id;
        $dat = Downline::with(['user', 'downline'])->where('user_id', $id)->count();
        $data = ['Total' => $dat];
        return ApiController::success('Total pengguna referral', $data);
    }
    public function downlinebot()
    {
        $id = auth()->user()->id;
        $data = Downline::with(['downlinebot'])->where('user_id', $id)->get();
        return ApiController::success('Total pengguna referral', $data);
    }
}
