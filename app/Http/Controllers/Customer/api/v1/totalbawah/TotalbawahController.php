<?php

namespace App\Http\Controllers\Customer\api\v1\totalbawah;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Totalbawah;
use Illuminate\Http\Request;

class TotalbawahController extends ApiController
{
    public function list()
    {
        $id = auth()->user()->id;
        // $id = 3;
        $data = Totalbawah::where('user_up', $id)->get();
        return ApiController::success('Berhasil Get list', $data);
    }
    public function count()
    {
        $id = auth()->user()->id;
        // $id = 3;
        $total = Totalbawah::where('user_up', $id)->count();
        $level1 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 1);
        })->count();
        $level2 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 2);
        })->count();;
        $level3 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 3);
        })->count();;
        $level4 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 4);
        })->count();;
        $level5 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 5);
        })->count();;
        $level6 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 6);
        })->count();;


        $data = [
            'total' => $total,
            'level1' => $level1,
            'level2' => $level2,
            'level3' => $level3,
            'level4' => $level4,
            'level5' => $level5,
            'level6' => $level6,
        ];

        return ApiController::success('Berhasil Get count', $data);
    }
}
