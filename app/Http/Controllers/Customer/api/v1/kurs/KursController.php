<?php

namespace App\Http\Controllers\Customer\api\v1\kurs;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Kurs;
use Illuminate\Http\Request;

class KursController extends ApiController
{
    public function kurs()
    {
        $data = Kurs::get();

        return ApiController::success('Data Kurs', $data);
    }
}
