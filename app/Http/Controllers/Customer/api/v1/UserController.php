<?php

namespace App\Http\Controllers\Customer\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Images;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Rules\Password;
use laravel\Sanctum\HasApiTokens;

class UserController extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function fetch()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $level = User::with(['leveluser'])->where('id', auth()->user()->id)->first();
        $role = auth()->user()->roles[0];
        $user['level'] = $level->leveluser;
        $user['role'] = $role;
        // foreach ($user as $key => $value) {
        if ($user->image_id) {
            $user['image_url'] = Images::where('id', $user->image_id)->first()->image_url;
        }
        $user->level['image_url'] = Images::where('id', $user->level->image_id)->first()->image_url;

        // }

        return ApiController::success('Data profile user berhasil diambil', $user);
    }
    public function refflink()
    {
        $path_url = "https://dailypips.co/register/";
        $dat = $path_url . auth()->user()->refferal;
        if (auth()->user()->level_id < 3) {
            // $data = ['reflink' => 'maaf tidak dapat menampilkan refflink'];
            $data = ['reflink' => $dat];
        } else {
            $data = ['reflink' => $dat];
        }
        return ApiController::success('Data profile user berhasil diambil', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return ApiController::failure(

                    "Email Not Found",

                    "Email Not Found",
                    'Authentication Failed',
                    500
                );
            }
            if (!Hash::check($request->password, $user->password, [])) {
                // throw new \Exception('Invalid Credentials');
                return ApiController::failure(

                    " Wrong Password ",

                    " Wrong Password ",
                    'Authentication Failed',
                    500
                );
            }
            // auth()->user()->currentAccessToken()->delete();
            $user->tokens()->where('tokenable_id', $user->id)->delete();

            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return ApiController::failure(
                    'Unauthorized',
                    'Authentication Failed',
                    500
                );
            }


            $level = User::with(['leveluser'])->where('id', auth()->user()->id)->first();
            $role = auth()->user()->roles[0];
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            $user['access_token'] = $tokenResult;
            $user['token_type'] = 'Bearer';
            $user['level'] = $level->leveluser;
            $user['role'] = $role;
            if ($user->image_id) {
                $user['image_url'] = Images::where('id', $user->image_id)->first()->image_url;
            }
            $user->level['image_url'] = Images::where('id', $user->level->image_id)->first()->image_url;


            return ApiController::success('', $user, 'Authenticated');
        } catch (Exception $error) {
            $user['level_url'] = Images::where('id', $user->level->image_id)->first()->image_url;
            return ApiController::failure(
                'Something went wrong',

                'Authentication Failed',
                500
            );
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        try {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['nullable', 'string', 'max:255'],
                'password' => ['required', 'string', new Password]
            ]);

            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]);


            $user = User::where('email', $request->email)->first();

            $tokenResult = $user->createToken('authToken')->plainTextToken;

            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ], 'User Registered');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Authentication Failed', 500);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();

        return ApiController::success('Token Revoked', $token);
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();

        $user = Auth::user();
        $user->update($data);

        return ApiController::success('Profile Updated', $user);
    }
}
