<?php

namespace App\Http\Controllers\Customer\api\v1\profit;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Keuntungan;
use Illuminate\Http\Request;

class ProfitController extends ApiController
{
    public function profit(Request $request)
    {
        $data = Keuntungan::orderby('tanggal', 'DESC')->limit(30)->get();
        return ApiController::success('Data Profit Berhasil Diambil', $data);
    }
}
