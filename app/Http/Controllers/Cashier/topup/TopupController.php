<?php

namespace App\Http\Controllers\Cashier\topup;

use App\Helper\LevelUserHelper;
use App\Http\Controllers\Controller;
use App\Models\Kurs;
use App\Models\Topup;
use App\Models\User;
use App\Models\Walletmt5;
use App\Models\Walletmt5history;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;




class TopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function indexmt5()
    {
        $selectmt5 = User::role('Customer')->with(['mt5'])->get();
        // $selectmt5 = DB::SELECT("select * from walletmt5s a, users b where a.user_id=b.id;");

        // dd($selectmt5);
        return view('page.dashboard.cashier.topup.topupmt5.index', compact(['selectmt5']));
    }

    public function tambah(Request $request)
    {
        $usrid = $request->id;
        $balance = $request->balance;
        $idwal = $request->idb;
        $idmt5 = DB::select("select id from walletmt5s where user_id=$usrid");
        $balanceawal = DB::select("select balance from walletmt5s where user_id=$usrid");
        $balanceawalmodal = DB::select("select modal_user from users where id=$usrid");


        $wallet = DB::table('walletmt5s')->where('user_id', $usrid)->update([
            'balance' => $balanceawal[0]->balance + (int)$balance,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);

        $mu = DB::table('users')->where('id', $usrid)->update([
            'modal_user' => $balanceawalmodal[0]->modal_user + (int)$balance,
            'updated_at' => Carbon::now(),
        ]);


        $data = Walletmt5history::create([
            'user_id' => $usrid,
            'wallet_id' => (int)$idmt5,
            'hystory_type_id' => '4',
            'time' => Carbon::now(),
            'note' => 'Admin topupmt5',
            'amount' => $balance,
            'status' => '15',
            'token' => Str::random(60),
        ]);


        return redirect()->route('dashboard.cashier.topup.topupmt5.index')->with('berhasil', '.');
    }

    public function tambahmodal(Request $request)
    {
        $usrid = $request->id;
        // dd($balanceawalmodal[0]->modal_user);
        $modalrp = $request->modalrupiah;
        $modaldl = $request->modal;
        $getkurs = Kurs::find(1)->harga_jual;
        $hasil = round($modalrp / $getkurs, 2);

        if ($request->modalrupiah) {
            $mu = DB::table('users')->where('id', $usrid)->update([
                'modal_user' => $hasil,
                'updated_at' => Carbon::now(),
            ]);
        } else {
            $mu = DB::table('users')->where('id', $usrid)->update([
                'modal_user' => $modaldl,
                'updated_at' => Carbon::now(),
            ]);
        }


        return redirect()->route('dashboard.cashier.topup.topupmt5.index')->with('berhasil', '.');
    }
}
