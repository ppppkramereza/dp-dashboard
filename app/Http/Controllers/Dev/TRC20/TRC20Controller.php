<?php

namespace App\Http\Controllers\Dev\TRC20;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TRC20Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Http::get('https://api.trongrid.io/v1/accounts/TMPvk6QpBcgeXkQbDTGfXt9Y3rS2pS6NTj/transactions/trc20?limit=100&only_to=true');
        $data = json_decode($data->getBody()->getContents())->data;
        $datas = array_search('TR2YRbSaCzdtP5GZHdXmekmuPb6CVJJbwp', array_column($data, 'from'));
        $data = [$data[$datas]];
        // dd($data);
        // $data = array(
        //     'transaksi' => $request->transaction_id,
        //     'block' => $request->block_timestamp,
        //     'from' => $request->from,
        //     'to' => $request->to,
        //     'type' => $request->type,
        //     'value' => $request->value

        //     );
        return view('page.dashboard.dev.TRC20.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
