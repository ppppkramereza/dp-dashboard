<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Kurs;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
class KursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        // $datas = DB::table('tabel_banks')->select('id','bank_name','bank_code','account_name',
        //     'account_number','created_at','updated')->get();

        $datas = Kurs::orderBy('created_at','DESC')->get();
        //$datas = User::all();

        Session::put('menu','masterdata');
        Session::put('sub','kurs');
        return view('page.dashboard.dev.kurs.index', compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = Kurs::create([
                'nama' => $request->input('nama'),
                'harga_jual' => $request->input('harga_jual'),
                'harga_beli' => $request->input('harga_beli'),
                ]);
        return redirect()->route('dashboard.dev.kurs.index')->with('berhasil','.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            Kurs::where('id', $request->id)->update([  
                'nama' => $request->input('nama'),
                'harga_jual' => $request->input('harga_jual'),
                'harga_beli' => $request->input('harga_beli'),
                ]);
        
     return redirect()->route('dashboard.dev.kurs.index')->with('berhasilupdate','.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kurs::find($id);
        $data->delete();
    
        return redirect()->route('dashboard.dev.kurs.index')->with('hapus', '.');
    }


}
