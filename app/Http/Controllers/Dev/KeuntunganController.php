<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\Keuntungan;
use App\Models\Walletmt5;
use App\Models\Walletmt5history;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class KeuntunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Keuntungan::where('status_generate', '0')->get();

        return view('page.dashboard.dev.keuntungan.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Keuntungan::create([
            'tanggal' => $request->input('tanggal'),
            'persen' => $request->input('persen'),
            'status_generate' => '0',
        ]);

        return redirect()->route('dashboard.dev.keuntungan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generate($id)
    {
        Keuntungan::where('id', $id)->update([
            'status_generate' => '1'
        ]);

        $keuntungan = Keuntungan::where('id', $id)->first();

        $wallet = Walletmt5::all();

        foreach ($wallet as $key) {

            $getWallet = Walletmt5::where('id', $key->id)->first();

            $time = Carbon::now();
            $token = Str::random(60);

            $saldo = $getWallet->balance;
            $persen = $keuntungan->persen;

            Walletmt5::where('id', $key->id)->update([
                'balance' => $saldo + round(floor($saldo) * ($persen / 100)),
                'last_update' => $time,
                'last_token' => $token
            ]);

            if (round(floor($saldo) * ($persen / 100)) > 0) {
                Walletmt5history::create([
                    'user_id' => $getWallet->user_id,
                    'wallet_id' => $getWallet->id,
                    'hystory_type_id' => 2,
                    'status' => 13,
                    'time' => $time,
                    'amount' => round(floor($saldo) * ($persen / 100)),
                    'note' => 'Profit Keuntungan',
                    'token' => $token
                ]);
            }
        }
        // $data = Keuntungan::find($id);

        // dd($data);

        return redirect()->route('dashboard.dev.keuntungan.index')->with('berhasil', '.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Keuntungan::where('id', $id)->update([
            'tanggal' => $request->input('tanggal'),
            'persen' => $request->input('persen'),
        ]);

        return redirect()->route('dashboard.dev.keuntungan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Keuntungan::find($id);
        $data->delete();

        return redirect()->route('dashboard.dev.keuntungan.index');
    }
}
