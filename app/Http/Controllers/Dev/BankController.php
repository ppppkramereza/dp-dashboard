<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\TabelBank;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        // $datas = DB::table('tabel_banks')->select('id','bank_name','bank_code','account_name',
        //     'account_number','created_at','updated')->get();

        $datas = TabelBank::orderBy('created_at', 'DESC')->get();
        //$datas = User::all();
        Session::put('menu','masterdata');
        Session::put('sub','bank');
        return view('page.dashboard.dev.bank.index', compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = TabelBank::create([
                'bank_name' => $request->input('nama'),
                'bank_code' => $request->input('kode'),
                'account_name' => $request->input('nama_akun'),
                'account_number' => $request->input('no_rek'),
                ]);
        return redirect()->route('dashboard.dev.bank.index')->with('berhasil','.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        TabelBank::where('id', $request->id)->update([
                'bank_name' => $request->input('nama'),
                'bank_code' => $request->input('kode'),
                'account_name' => $request->input('nama_akun'),
                'account_number' => $request->input('no_rek'),
                ]);
        
     return redirect()->route('dashboard.dev.bank.index')->with('berhasilupdate','.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TabelBank::find($id);
        $data->delete();
    
        return redirect()->route('dashboard.dev.bank.index')->with('hapus', '.');
    }

}
